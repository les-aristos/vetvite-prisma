const {forwardTo} = require('prisma-binding');

async function availability(parent, args, ctx, info) {
    return forwardTo('prisma')(parent, args, ctx, info);
}

async function availabilities(parent, args, ctx, info) {
    return forwardTo('prisma')(parent, args, ctx, info);
}   

module.exports = {
    availability,
    availabilities
}