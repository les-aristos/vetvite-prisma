const {createCareType, updateCareType, deleteCareType} = require('./mutations/careTypeMutation');
const availabilityMutation = require('./mutations/availabilityMutation');
const unavailabilityMutation = require('./mutations/unavailabilityMutation');

const Mutation = {
    createCareType,
    updateCareType,
    deleteCareType,
    ...availabilityMutation,
    ...unavailabilityMutation
};

module.exports = {
    Mutation
}