const { forwardTo } = require('prisma-binding');

async function createCareType(parent, args, ctx, info) {
  return forwardTo('prisma')(parent, args, ctx, info)
}

async function updateCareType(parent, args, ctx, info) {
    return forwardTo('prisma')(parent, args, ctx, info)
}

async function deleteCareType(parent, args, ctx, info) {
    return forwardTo('prisma')(parent, args, ctx, info)
}

module.exports = {
    createCareType,
    updateCareType,
    deleteCareType
}