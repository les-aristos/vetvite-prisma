const {careType, careTypes} = require('./queries/careTypeQuery');
const availabilityQuery = require('./queries/availabilityQuery');
const unavailabilityQuery = require('./queries/unavailabilityQuery');

const Query = {
    careType,
    careTypes,
    ...availabilityQuery,
    ...unavailabilityQuery
}

module.exports = {
    Query
}